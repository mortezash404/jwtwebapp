﻿namespace JwtWebApp.Models
{
    public class JwtSetting
    {
        public string ValidAudience { get; set; }
        public string ValidIssuer { get; set; }
        public string Key { get; set; }
    }
}
